<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/bs3/img/dancok.png')}}">
    <title>login</title>
    <link href="{{asset('assets/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/bs3/css/font-awesome.css')}}" rel="stylesheet">
</head>
<body style="background-image: url('assets/bs3/img/buku6.png'); background-repeat: no-repeat; background-size: cover;">
    <div class="main-wrapper">
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
            <div class="auth-box bg-dark border-secondary" style="margin-top: 0px; margin-bottom: 45px;">
                @yield('content')
            </div>
        </div>
    </div>
    @yield('customscript')
</body>
</html>
