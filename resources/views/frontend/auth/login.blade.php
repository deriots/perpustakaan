@extends('layouts.app')

@section('content')
    <div id="loginform">
        <div class="text-center p-t-20 p-b-20">
            <img src="{{asset('assets/bs3/img/user.png')}}" style="width: 100px; height: 100px;"><br>
            <label for="" style="font-family: monospace; font-size: 25px; color: ivory">Login</label>
        </div>
        <!-- Form -->
        <form class="form-horizontal m-t-20" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
            @csrf
            <div class="row p-b-30">
                <div class="col-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
                        </div>
                        <input type="email" id="email"  name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ old('email') }}" placeholder="Email" aria-label="Username" aria-describedby="basic-addon1" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-lock"></i></span>
                        </div>
                        <input type="password" id="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                        placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember" style="color: ivory">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row border-top border-secondary">
                <div class="col-12">
                    <div class="form-group">
                        <div class="p-t-20">
                            <br>
                            <button type="submit" class="btn btn-success float-left">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div><br>
            <p class="text-inverse text-left text-center" style="color: white;">Don't have an account?
                <a href="{{route('register')}}"> <b>Register here </b></a>for free!
            </p>
        </form>
    </div>
@endsection

@section('customscript')
    <script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    $('#to-login').click(function(){

        $("#recoverform").hide();
        $("#loginform").fadeIn();
    });
    </script>
@endsection
</body>
</html>
