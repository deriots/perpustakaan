@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>List Peminjaman</h3></b>
@endsection

@section('content')
    <a href="{{route('peminjaman.create')}}" class="btn btn-success fa fa-plus"> Tambah</a><br><br>
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">NO</th>
                        <th style="text-align: center;">Code</th>
                        <th style="text-align: center;">Anggota Perpus</th>
                        <th style="text-align: center;">User</th>
                        <th style="text-align: center;">Tanggal Pinjam</th>
                        <th style="text-align: center;">Estimasi Tanggal Kembali</th>
                        <th style="text-align: center;">Jumlah Buku</th>
                        <th style="width:15%; text-align:center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($peminjaman as $peminjaman)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{$peminjaman['code']}}</td>
                        <td>{{$peminjaman['cuk']}}</td>
                        <td>{{$peminjaman['user']}}</td>
                        <td>{{$peminjaman['loan_date']->format('d-m-Y')}}</td>
                        <td>{{$peminjaman['estimated_return_date']->format('d-m-Y')}}</td>
                        <td>{{$peminjaman['number_of_books']}}</td>
                        <td>
                            <center>
                            <table>
                                <tr>
                                    <a href="{{route('peminjaman.show', $peminjaman->id)}}">
                                        <button id="btndetail" class="btn btn-info fa fa-eye" style="padding-bottom: 3px; padding-top: 3px;" type="submit">
                                        </button>
                                    </a>
                                    <a href="{{route('peminjaman.edit', $peminjaman->id)}}">
                                        <button class="btn btn-warning fa fa-edit" style="padding-bottom: 3px; padding-top: 3px; width: 37px;" type="submit">
                                        </button>
                                    </a>
                                    <form action="{{action('PeminjamanController@destroy', $peminjaman->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger fa fa-trash" type="submit" style="padding-bottom: 3px; padding-top: 3px;"></button>
                                    </form>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
