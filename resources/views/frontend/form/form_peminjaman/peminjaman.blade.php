@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Peminjaman</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{url('peminjaman')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body"><br>
                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="code" class="control-label col-form-label">Kode Peminjaman</label>
                                    <div class="input-group-append">
                                    <input type="text" name="code" class="form-control" placeholder="Masukkan Code" id="code" value="{{$code}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="member_id" class="control-label col-form-label">Anggota Perpus</label>
                                    <div class="input-group-append">
                                        <select class="form-control" name="member_id" id="anggota">
                                            <option selected>--Pilih Anggota--</option>
                                            @foreach ($anggota_perpus as $p)
                                                <option value="{{ $p->id }}">{{ $p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="user_id" class="text-right control-label col-form-label">User</label>
                                    <div class="input-group-append">
                                         {{-- <span class="caret"></span> --}}
                                        <input type="text" name="user_id" class="form-control" id="user" value="{{ Auth::user()->name }}" readonly>
                                        <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="loan_date" class="text-right control-label col-form-label">Tanggal Pinjam</label>
                                    <div class="input-group-append">
                                        <input type="date" name="loan_date" class="form-control" id="loan_date" value="{{ date ('Y-m-d')}}" readonly>
                                        <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="estimated_return_date" class="text-right control-label col-form-label">Estimasi Tanggal Kembali</label>
                                    <div class="input-group-append">
                                        <input type="date" name="estimated_return_date" class="form-control" id="estimated_return_date" value="{{ \Carbon\Carbon::now()->addDay('3')->format('Y-m-d') }}" readonly>
                                        <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="number_of_books" class="text-right control-label col-form-label">Jumlah Buku</label>
                                    <div class="input-group-append">
                                        <input type="text" name="number_of_books" class="form-control" placeholder="Masukkan Jumlah Buku" id="number_of_books">
                                    </div>
                                </div>
                            </div><br><hr>

                            {{-- Detail Peminjaman --}}
                            <div class="row col-md-12">
                                <div class="col-md-3">
                                <h4>Detail Peminjaman</h4>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-3">
                                    <label for="buku" class="text-right control-label col-form-label">Buku</label>
                                    <div class="input-group-append">
                                        <select class="form-control" name="buku" id="buku">
                                            <option selected value="">--Pilih Buku--</option>
                                            @foreach ($bukus as $p)
                                                <option value="{{ $p->id }}"
                                                    data-nobuku="{{ $p->book_number}}"
                                                    data-jbuku="{{ $p->book_title}}"
                                                    data-pbuku="{{ $p->author}}"
                                                    data-cbuku="{{ $p->category}}"
                                                    >{{ $p->book_title}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3" style=" margin-top: 35px;">
                                    <button type="button" id="btnadd" class="btn btn-primary" style="border-radius: 50px;">Add</button>
                                </div>
                            </div><br><br>
                            {{-- Detail Peminjaman --}}

                            {{-- table --}}
                            <div class="row col-md-12">
                                <div class="col-md-12">
                                    <table class="table table-hover dataTable no-footer">
                                        <thead class="thead-dark">
                                            <tr role="row">
                                                <th style="text-align: center; width: ">Book Number</th>
                                                <th style="text-align: center;">Book Title</th>
                                                <th style="text-align: center;">Author</th>
                                                <th style="text-align: center;">Category</th>
                                                <th style="text-align: center;">Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody-detail">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {{-- table --}}

                            {{-- button --}}
                            <div class="border-top">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success" style="border-radius: 30px;">Submit</button>
                                            <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                                                <a href="{{route('peminjaman.index')}}" style="color: white;"> Kembali</a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- button --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customscript')
    <script>
        var arrayId = [];
        var i = 0;
        var trHtml = '';

        $(document).ready(function(){

            $('#btnadd').click(function() {
                // alert($(this).val());
                // $(this).val();
                var idbuku = $('#buku').find(':selected').val();

                if ( $('#buku').val() == null || $('#buku').val() == "") {
                    alert('buku harus di isi!');
                    return false;
                }

                $.ajax({
                    url : "{{ url('get-item') }}/" + idbuku,
                    method : "GET",
                    success : function(result){
                        console.log(result);

                        var status = false;
                        var id = result.id;

                        for(var i=0; i<arrayId.length; i++){
                            var valarr = arrayId[i];
                            if( valarr == id){
                                status = true;
                                break;
                            }
                        }

                        if( status == true){
                            alert('Data Sama!');
                            return false;
                        }else{
                            arrayId.push(result.id);
                            generateRow(result);
                        }

                    }

                });
            });
        });

        function generateRow(result){

            trHtml = '<tr id="tr_'+result.id+'">';
                trHtml += '<td>';
                    trHtml += '<input type="text" value="'+result.book_number+'" readonly="" class="form-control input-sm">';
                    trHtml += '<input type="hidden" name="bukuid[]" id="bukuid_'+result.id+'" value="'+result.id+'"class="form-control input-sm">';
                trHtml += '</td>';

                trHtml += '<td>';
                    trHtml += '<input type="text" value="'+result.book_title+'" readonly="" class="form-control input-sm">';
                trHtml += '</td>';

                trHtml += '<td>';
                    trHtml += '<input type="text" value="'+result.author+'" readonly="" class="form-control input-sm">';
                trHtml += '</td>';

                trHtml += '<td>';
                    trHtml += '<input type="text" value="'+result.category+'"  readonly="" class="form-control input-sm">';
                trHtml += '</td>';

                trHtml += '<td>';
                    trHtml += '<table>';
                        trHtml += '<tr>';

                            // trHtml += '<td  style="border-top: 0px;">';
                                trHtml += '<button type="button" class="btn btn-danger" style="width: 35px; height: 33px;" onclick="deletedRow('+result.id+')" ><i class="fa fa-trash"></i></button>';
                            // trHtml += '</td>';

                        trHtml += '</tr>';
                    trHtml += '</table>';
                trHtml += '</td>';
            trHtml += '</tr>';

            //add-to-html
            $('#tbody-detail').append(trHtml);

            //reset-field-buku
            $('#buku').val('').trigger('change');
        }

        function deletedRow(id) {
            if( confirm("Hapus data ?") ){
                arrayId.splice( $.inArray($('#produk_'+id).val(),arrayId) ,1 );
                console.log(arrayId);

                $('#tbody-detail #tr_'+$('#bukuid_'+id).val()).remove();
            }
            return false;
        }
    </script>
@endsection