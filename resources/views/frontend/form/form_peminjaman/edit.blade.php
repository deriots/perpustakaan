@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Edit Peminjaman</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{action('PeminjamanController@update',$id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="card-body"><br>
                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="code" class="control-label col-form-label">Code</label>
                                    <div class="input-group-append">
                                    <input type="text" name="code" class="form-control" placeholder="Masukkan Code" id="code" value="{{$peminjaman->code}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="member_id" class="control-label col-form-label">Anggota</label>
                                    <div class="input-group-append">
                                        <select class="form-control" name="member_id" id="anggota">
                                            <option selected>--Pilih Anggota--</option>
                                            @foreach ($anggota_perpus as $p)
                                                <option @if ($peminjaman->member_id == $p->id) selected @endif value="{{ $p->id }}">{{ $p->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="user_id" class="text-right control-label col-form-label">User</label>
                                    <div class="input-group-append">
                                         {{-- <span class="caret"></span> --}}
                                        <input type="text" name="user_id" class="form-control" id="user" value="{{ Auth::user()->name }}" readonly>
                                        <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="loan_date" class="text-right control-label col-form-label">Tanggal Pinjam</label>
                                    <div class="input-group-append">
                                        <input type="date" name="loan_date" class="form-control" id="loan_date" value="{{ $peminjaman->loan_date->format ('Y-m-d') }}" readonly>
                                        <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="estimated_return_date" class="text-right control-label col-form-label">Estimasi Tanggal Kembali</label>
                                    <div class="input-group-append">
                                        <input type="date" name="estimated_return_date" class="form-control" id="estimated_return_date" value="{{  $peminjaman->loan_date->addDay('3')->format('Y-m-d') }}" readonly>
                                        <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="number_of_books" class="text-right control-label col-form-label">Jumlah Buku</label>
                                    <div class="input-group-append">
                                        <input type="text" name="number_of_books" class="form-control" placeholder="Masukkan Jumlah Buku" id="number_of_books" value="{{$peminjaman->number_of_books}}">
                                    </div>
                                </div>
                            </div><br>
                            {{-- button --}}
                            <div class="border-top">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-warning" style="border-radius: 30px;">Edit</button>
                                            <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                                                <a href="{{route('peminjaman.index')}}" style="color: white;"> Kembali</a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- button --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

