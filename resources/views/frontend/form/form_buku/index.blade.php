@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>List Buku</h3></b>
@endsection

@section('content')
    <a href="{{route('bukus.create')}}" class="btn btn-success fa fa-plus"> Tambah</a>
    <br><br>
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br>
        @endif
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">NO</th>
                        <th style="text-align: center;">Book Number</th>
                        <th style="text-align: center;">Book Title</th>
                        <th style="text-align: center;">Author</th>
                        <th style="text-align: center;">Category</th>
                        <th style="width:11%; text-align: center;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bukus as $buku)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{$buku['book_number']}}</td>
                        <td>{{$buku['book_title']}}</td>
                        <td>{{$buku['author']}}</td>
                        <td>{{$buku['cok']}}</td>
                        <td>
                            <center>
                            <table>
                                <tr>
                                    <a href="{{route('bukus.edit', $buku->id)}}">
                                        <button class="btn btn-warning fa fa-edit" style="padding-bottom: 3px; padding-top: 3px; width: 37px;" type="submit">
                                        </button>
                                    </a>
                                    <form action="{{action('BukuController@destroy', $buku->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger fa fa-trash" type="submit" style="padding-bottom: 3px; padding-top: 3px;"></button>
                                    </form>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
