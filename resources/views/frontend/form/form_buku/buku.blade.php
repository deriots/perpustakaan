@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Buku</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{url('bukus')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <h4 class="card-title">Form Buku</h4>
                            <div class="form-group row">
                                <label for="book_number" class="col-sm-3 text-right control-label col-form-label">Book Number</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="book_number" class="form-control" placeholder="Masukkan Nomor Buku" id="book_number">
                                    <span class="input-group-text fa fa-pencil" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="book_title" class="col-sm-3 text-right control-label col-form-label">Book Title</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="book_title" class="form-control" placeholder="Masukkan Judul Buku" id="book_title">
                                    <span class="input-group-text fa fa-book" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="author" class="col-sm-3 text-right control-label col-form-label">Author</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="author" class="form-control" placeholder="Masukkan Pengarang" id="author">
                                    <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="category" class="col-sm-3 text-right control-label col-form-label">Category</label>
                                <div class="col-sm-9 input-group-append">
                                    <select class="form-control" name="category" id="category">
                                        <option selected>--Pilih Kategori--</option>
                                        @foreach ($kategori_buku as $p)
                                            <option value="{{ $p->id }}">{{ $p->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="input-group-text fa fa-list-alt" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success" style="border-radius: 50px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection