@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>List Anggota Perpustakaan</h3></b>
@endsection

@section('content')
    <a href="{{route('anggota_perpuses.create')}}" class="btn btn-success fa fa-plus"> Tambah</a>
    <br><br>
        @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br>
        @endif
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">NO</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Date Of Birth</th>
                        <th style="text-align: center;">Address</th>
                        <th style="text-align: center;">Gender</th>
                        <th style="text-align: center;">No Telphone</th>
                        <th style="width:11%; text-align: center;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($anggota_perpuses as $anggota_perpus)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{$anggota_perpus->name}}</td>
                        <td>{{$anggota_perpus->date_of_birth}}</td>
                        <td>{{$anggota_perpus->address}}</td>
                        <td>{{$anggota_perpus->gender}}</td>
                        <td>{{$anggota_perpus->no_telphone}}</td>
                        <td>
                            <center>
                            <table>
                                <tr>
                                    <a href="{{route('anggota_perpuses.edit', $anggota_perpus->id)}}">
                                        <button class="btn btn-warning fa fa-edit" style="padding-bottom: 3px; padding-top: 3px; width: 37px;" type="submit">
                                        </button>
                                    </a>
                                    <form action="{{action('Anggota_PerpusController@destroy', $anggota_perpus->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger fa fa-trash" type="submit" style="padding-bottom: 3px; padding-top: 3px;"></button>
                                    </form>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
