@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Anggota Perpustakaan</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{url('anggota_perpuses')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <h4 class="card-title">Form Anggota Perpustakaan</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" id="name">
                                    <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date_of_birth" class="col-sm-3 text-right control-label col-form-label">Date Of Birth</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="date" name="date_of_birth" class="form-control" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" min="1980-01-01">
                                    <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-sm-3 text-right control-label col-form-label">Address</label>
                                <div class="col-sm-9 input-group-append">
                                    <textarea name="address" id="address" class="form-control" placeholder="Masukkan Alamat"></textarea>
                                    <span class="input-group-text fa fa-globe" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label radio">Gender</label>
                                <div class="col-sm-9 input-group-append">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="gender" id="male" value="male"> Male</label><br>
                                        <label><input type="checkbox" name="gender" id="female" value="female"> Female</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_telphone" class="col-sm-3 text-right control-label col-form-label radio" value="no_telphone">No Telephone</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="no_telphone" class="form-control txtboxToFilter" placeholder="Masukkan No Telphone" id="no_telphone">
                                    <span class="input-group-text fa fa-phone" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success" style="border-radius: 50px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customscript')
    <script>
        $(document).ready(function() {
            $(".txtboxToFilter").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl/cmd+A
                    (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+C
                    (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: Ctrl/cmd+X
                    (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection