@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Edit Buku</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{action('Anggota_PerpusController@update',$id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="card-body">
                            <h4 class="card-title">Form Kategori</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" id="name"
                                    value="{{$anggota_perpus->name}}">
                                    <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date_of_birth" class="col-sm-3 text-right control-label col-form-label">Date Of Birth</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="date" name="date_of_birth" class="form-control" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"
                                    min="1990-01-01" value="{{$anggota_perpus->date_of_birth}}">
                                    <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-sm-3 text-right control-label col-form-label">Address</label>
                                <div class="col-sm-9 input-group-append">
                                    <textarea name="address" id="address" class="form-control" placeholder="Masukkan Alamat">{{$anggota_perpus->address}}</textarea>
                                    <span class="input-group-text fa fa-globe" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-right control-label col-form-label radio">Gender</label>
                                <div class="col-sm-9 input-group-append">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="gender" id="male" value="male" @if( $anggota_perpus->gender  == 'male') checked @endif> Male</label><br>
                                        <label><input type="checkbox" name="gender" id="female" value="female" @if( $anggota_perpus->gender  == 'female') checked @endif> Female</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="no_telphone" class="col-sm-3 text-right control-label col-form-label radio" value="male">No Telephone</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="number" name="no_telphone" class="form-control txtboxToFilter" placeholder="Masukkan No Telphone"
                                    value="{{$anggota_perpus->no_telphone}}" id="no_telphone">
                                    <span class="input-group-text fa fa-phone" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            {{-- button --}}
                            <div class="border-top">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-warning" style="border-radius: 30px;">Edit</button>
                                            <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                                                <a href="{{route('anggota_perpuses.index')}}" style="color: white;"> Kembali</a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- button --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection