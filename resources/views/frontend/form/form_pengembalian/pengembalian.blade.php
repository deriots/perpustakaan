@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Pengembalian</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{url('pengembalian')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body"><br>
                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="loan_code" class="control-label col-form-label">Kode Peminjaman</label>
                                    <div class="input-group-append">
                                        <select class="form-control" name="loan_code" id="pcode">
                                        <option selected value="">--Pilih Kode--</option>
                                        @foreach ($peminjaman as $p)
                                            <option value="{{ $p->code }}">{{ $p->code}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="return_code" class="control-label col-form-label">Kode Pengembalian</label>
                                    <div class="input-group-append">
                                    <input type="text" name="return_code" class="form-control" id="return_code" value="{{$return_code}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="id_user" class="text-right control-label col-form-label">User</label>
                                    <div class="input-group-append">
                                       <input type="text" name="id_user" class="form-control" id="user" value="{{ Auth::user()->name }}" readonly>
                                        <span class="input-group-text fa fa-user" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                            </div><br>

                            <div class="row col-md-12">
                                <div class="col-md-4">
                                    <label for="date_back" class="text-right control-label col-form-label">Tanggal Kembali</label>
                                    <div class="input-group-append">
                                        <input type="date" name="date_back" class="form-control" id="tanggal_kembali" value="" readonly>
                                        <span class="input-group-text fa fa-calendar" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="number_of_books" class="text-right control-label col-form-label">Jumlah Buku</label>
                                    <div class="input-group-append">
                                        <input type="text" name="number_of_books" class="form-control" placeholder="Masukkan Jumlah Buku" id="number_of_books">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="denda" class="text-right control-label col-form-label">Denda</label>
                                    <div class="input-group-append">
                                        <input type="text" name="denda" class="form-control" readonly id="denda">
                                    </div>
                                </div>
                            </div><br>
                            {{-- button --}}
                            <div class="border-top">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-success" style="border-radius: 30px;">Submit</button>
                                            <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                                                <a href="{{route('pengembalian.index')}}" style="color: white;"> Kembali</a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- button --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('customscript')
    <script>
        $(document).ready(function () {

            $('#pcode').on('change', function () {
                var pcode = $(this).val();
                // alert('hallo');
                if ( $('#pcode').val() == null || $('#pcode').val() == "") {
                    alert('kode harus di isi!');
                    return false;
                }

                $.ajax({
                    url :  "{{ url('get-pengembalian') }}/" + pcode,
                    method : "GET",
                    success : function (result) {
                        // console.log(result);

                        var today= new Date();
                        var fine = null;
                        var formattedDate = new Date(result.estimated_return_date);
                        var d = formattedDate.getDate();
                        var m =  formattedDate.getMonth();
                        m += 1;  // JavaScript months are 0-11
                        var y = formattedDate.getFullYear();

                        d = ("0" + d).slice(-2);
                        m = ("0" + m).slice(-2);

                        // console.log(formattedDate);
                        $('#tanggal_kembali').val(y + "-" + m + "-" + d );

                        if( formattedDate < today ){
                            // console.log('denda = 1000 * keterlambatan hari');
                            lateDays = Date.dateDiff('d', formattedDate, today);
                            //convert to +
                            lateDays = lateDays * -1;
                            fine = 2000 * lateDays;

                        }else{
                            fine = 0;
                        }

                        $('#denda').val(fine);

                    }
                })
            });
                //Set the two dates
                // var y2k  = new Date(2018, 10, 26);
                // var today= new Date();
        });
        Date.dateDiff =
        function(datepart, fromdate, todate) {
            datepart = datepart.toLowerCase();
            // var diff = todate - fromdate;
            var diff = fromdate - todate;
            var divideBy = { w:604800000,
                            d:86400000,
                            h:3600000,
                            n:60000,
                            s:1000 };

            return Math.floor( diff/divideBy[datepart]);
        }
    </script>
@endsection
