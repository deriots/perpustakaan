@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>List Pengembalian</h3></b>
@endsection

@section('content')
    <a href="{{route('pengembalian.create')}}" class="btn btn-success fa fa-plus"> Tambah</a><br><br>
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">NO</th>
                        <th style="text-align: center;">Loan Code</th>
                        <th style="text-align: center;">Return Code</th>
                        <th style="text-align: center;">User</th>
                        <th style="text-align: center;">Number Of Books</th>
                        <th style="text-align: center;">Date Back</th>
                        <th style="text-align: center;">Denda</th>
                        <th style="width:15%; text-align:center">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pengembalian as $pengembalian)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{$pengembalian['loan_code']}}</td>
                        <td>{{$pengembalian['return_code']}}</td>
                        <td>{{$pengembalian['user']}}</td>
                        <td>{{$pengembalian['number_of_books']}}</td>
                        <td>{{$pengembalian['date_back']->format('d-m-Y')}}</td>
                        <td>{{$pengembalian['denda']}}</td>
                        <td>
                            <center>
                            <table>
                                <tr>
                                    <a href="{{route('pengembalian.show', $pengembalian->id)}}">
                                        <button id="btndetail" class="btn btn-info fa fa-eye" style="padding-bottom: 3px; padding-top: 3px;" type="submit">
                                        </button>
                                    </a>
                                    <a href="{{route('pengembalian.edit', $pengembalian->id)}}">
                                        <button class="btn btn-warning fa fa-edit" style="padding-bottom: 3px; padding-top: 3px; width: 37px;" type="submit">
                                        </button>
                                    </a>
                                    <form action="{{action('PengembalianController@destroy', $pengembalian->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger fa fa-trash" type="submit" style="padding-bottom: 3px; padding-top: 3px;"></button>
                                    </form>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
