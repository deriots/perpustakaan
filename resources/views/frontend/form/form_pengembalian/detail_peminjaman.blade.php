@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Detail Peminjaman Buku</h3></b>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">NO</th>
                        <th style="text-align: center;">Book Number</th>
                        <th style="text-align: center;">Book Title</th>
                        <th style="text-align: center;">Author</th>
                        <th style="text-align: center;">Category</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detailpeminjaman as $detailpeminjaman)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{$detailpeminjaman['book_number']}}</td>
                        <td>{{$detailpeminjaman['book_title']}}</td>
                        <td>{{$detailpeminjaman['author']}}</td>
                        <td>{{$detailpeminjaman['category']}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table><hr>
            {{-- button --}}
            <div class="container-fluid">
                <div class="float-right">
                    <div class="row">
                        <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                            <a href="{{route('peminjaman.index')}}" style="color: white;"> Kembali</a>
                        </button>
                    </div>
                </div>
            </div>
            {{-- button --}}
        </div>
    </div>
@endsection
