@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Edit Kategori Buku</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{action('Kategori_BukuController@update',$id)}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('put') }}
                        <div class="card-body">
                            <h4 class="card-title">Form Kategori Buku</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Name Category</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" id="name" value="{{$kategori_buku->name}}">
                                    <span class="input-group-text fa fa-list-alt" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="description" class="form-control" placeholder="Masukkan Deskripsi" id="description" value="{{$kategori_buku->description}}">
                                    <span class="input-group-text fa fa-file-text" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>

                            {{-- button --}}
                            <div class="border-top">
                                <div class="float-right">
                                    <div class="row">
                                        <div class="card-body">
                                            <button type="submit" class="btn btn-warning" style="border-radius: 30px;">Edit</button>
                                            <button type="button" class="btn btn-cyan fa fa-arrow-left" style="height: 35px; border-radius: 30px;">
                                                <a href="{{route('kategori_bukus.index')}}" style="color: white;"> Kembali</a>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- button --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection