@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>List Kategori Buku</h3></b>
@endsection

@section('content')
    <a href="{{route('kategori_bukus.create')}}" class="btn btn-success fa fa-plus"> Tambah</a>
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div style="position: absolute; height: 1px; width: 3px; overflow: hidden;">
                <input type="text" tabindex="0">
            </div>
            <table class="table table-striped table-hover dataTable no-footer">
                <thead class="thead-dark">
                    <tr role="row">
                        <th style="width: 20px; text-align: center;">ID</th>
                        <th style="width: 20%; text-align: center;">Name Category</th>
                        <th style="text-align: center;">Description</th>
                        <th style="width:11%; text-align: center;">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($kategori_bukus as $Kategori_buku)
                    <tr>
                        <td>{{$loop->index + 1 }}</td>
                        <td>{{$Kategori_buku['name']}}</td>
                        <td>{{$Kategori_buku['description']}}</td>
                        <td>
                            <center>
                            <table>
                                <tr>
                                    <a href="{{route('kategori_bukus.edit', $Kategori_buku->id)}}">
                                        <button class="btn btn-warning fa fa-edit" style="padding-bottom: 3px; padding-top: 3px; width: 37px;" type="submit">
                                        </button>
                                    </a>
                                   <form action="{{action('Kategori_BukuController@destroy', $Kategori_buku->id)}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-danger fa fa-trash" type="submit" style="padding-bottom: 3px; padding-top: 3px;"></button>
                                    </form>
                                </tr>
                            </table>
                            </center>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
