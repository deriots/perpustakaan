@extends('frontend.template.index')

@section('contentheadertitle')
    <b><h3>Kategori Buku</h3></b>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="border-radius: 8px;">
                    <form class="form-horizontal" action="{{url('kategori_bukus')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <h4 class="card-title">Form Kategori Buku</h4>
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 text-right control-label col-form-label">Name Category</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="name" class="form-control" placeholder="Masukkan Nama" id="name">
                                    <span class="input-group-text fa fa-list-alt" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="description" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                <div class="col-sm-9 input-group-append">
                                    <input type="text" name="description" class="form-control" placeholder="Masukkan Deskripsi" id="description">
                                    <span class="input-group-text fa fa-file-text" style="padding-top: 10px; padding-bottom: 10px;"></span>
                                </div>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-success" style="border-radius: 50px;">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection