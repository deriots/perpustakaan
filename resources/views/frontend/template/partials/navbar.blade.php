<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-primary">
        <div class="navbar-header" data-logobg="skin5">
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                <!-- Logo -->
                <div class="navbar-brand">
                    <b class="logo-icon p-l-10">
                        <!-- Dark Logo icon -->
                        <img src="{{ asset('assets/bs3/img/buku5.png')}}" alt="homepage" class="light-logo" style="width: 30px; height: 39px; padding-bottom: 2zpx;">
                    </b>
                    <!-- Logo text -->
                    <span class="logo-text">
                        <!-- dark Logo text -->
                        <img src="{{ asset('assets/images/asu1.jpg')}}" >
                    </span>
                </div>
                <!-- End Logo -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>

        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- toggle and nav items -->
            <ul class="navbar-nav float-left mr-auto">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
            </ul>
            <!-- Right side toggle and nav items -->
            <ul class="navbar-nav float-right">
                <!-- User profile and search -->
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset('assets/bs3/img/user.png')}}" class="rounded-circle user-image" width="31" alt="User Image">
                    </a>
                    {{-- logout --}}
                    <div class="dropdown-menu dropdown-menu-right user-dd animated" aria-labelledby="navbarDropdown">
                        <div class="dropdown-divider"></div>
                        <div class="text-center" style="color: blue; font-size: 20px;">
                            <b>{{ Auth::user()->name }}</b>
                        </div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off m-r-5 m-l-5"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    {{-- akhir logout --}}
                </li>
                <!-- User profile and search -->
            </ul>
            <!-- Right side toggle and nav items -->
        </div>
    </nav>
</header>