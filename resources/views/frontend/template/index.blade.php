<!DOCTYPE html>
<html dir="ltr" lang="en">
    {{-- link --}}
    @include('frontend.template.partials.link')
    {{-- akhir link --}}
    <body>
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div id="main-wrapper">
            {{-- navbar --}}
            @include('frontend.template.partials.navbar')
            {{-- akhir navbar --}}

            {{-- side bar --}}
            @include('frontend.template.partials.sidebar')
            {{-- akhir side bar --}}

           <!-- Page wrapper  -->
            <div class="page-wrapper">
                <!-- Bread crumb and right sidebar toggle -->
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-12 d-flex no-block align-items-center">
                            <h4 class="page-title">@yield('contentheadertitle')</h4>
                        </div>
                    </div>
                </div>
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- Container fluid  -->
                <div class="container-fluid">
                    @yield('content')
                </div>
                <!-- End Container fluid  -->

                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <footer class="footer text-center">
                    Copyright © 2018-2019 by <a href="https://instagram.com">Ade</a>.
                </footer>
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->

            </div>
            <!-- End Page wrapper  -->
        </div>

        {{-- javacript --}}
        @include('frontend.template.partials.java_script')
        {{-- akhir javasript --}}
    </body>
</html>