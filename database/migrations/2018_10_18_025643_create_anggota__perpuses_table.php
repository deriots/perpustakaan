<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnggotaPerpusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota__perpuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',15);
            $table->string('date_of_birth',30);
            $table->string('address',50);
            $table->enum('gender', ['male', 'female']);
            $table->string('no_telphone',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota__perpuses');
    }
}
