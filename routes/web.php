<?php
Route::get('/', function () {
    return view('frontend.auth.login');
});
Auth::routes('home');

Route::get('home', 'HomeController@index')->name('home');

route::group(['middleware'=> ['auth']], function(){
    route::resource('kategori_bukus','Kategori_BukuController');
    route::resource('bukus','BukuController');
    route::resource('anggota_perpuses','Anggota_PerpusController');
    route::resource('peminjaman','PeminjamanController');
    route::resource('pengembalian', 'PengembalianController');
});

Route::get('get-item/{id}', 'PeminjamanController@getitem')->name('get.item');
Route::get('get-pengembalian/{id}', 'PengembalianController@getitem')->name('get.pengembalian');



