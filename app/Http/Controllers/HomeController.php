<?php

namespace App\Http\Controllers;

use App\Models\Master\Anggota_Perpus;
use App\Models\Master\Buku;
use App\Models\Transaksi\Peminjaman;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $anggota_perpus = Anggota_Perpus::orderBy('name','asc')->get();
        // $code = Peminjaman::Code();
        // $bukus = Buku::orderBy('book_title','asc')->get();
        // return view('frontend.form.form_peminjaman.peminjaman',compact('anggota_perpus','bukus','code'));
        return view('frontend.template.dashboard');
    }
}
