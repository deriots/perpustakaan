<?php

namespace App\Http\Controllers;

use App\Models\Master\Buku;
use App\Models\Master\Kategori_buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bukus = Buku::select('bukus.*','kategori_bukus.name as cok')
        ->join('kategori_bukus','kategori_bukus.id','=','bukus.category')
        ->get();
        return view('frontend.form.form_buku.index',compact('bukus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori_buku = Kategori_buku::orderBy('name','asc')->get();
        return view('frontend.form.form_buku.buku',compact('kategori_buku'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $buku = new Buku;
        $buku->book_number = $request->get('book_number');
        $buku->book_title = $request->get('book_title');
        $buku->author = $request->get('author');
        $buku->category = $request->get('category');
        $buku->save();

        return redirect('bukus')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $buku = Buku::find($id);
        $kategori_buku = Kategori_buku::orderBy('name','asc')->get();
        return view('frontend.form.form_buku.edit',compact('buku','id','kategori_buku'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $buku = Buku::find($id);
        $buku->book_number=$request->get('book_number');
        $buku->book_title=$request->get('book_title');
        $buku->author=$request->get('author');
        $buku->category=$request->get('category');
        $buku->save();

        return redirect('bukus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = Buku::find($id);
        $buku->delete();
        return redirect('bukus')->with('success');
    }
}
