<?php

namespace App\Http\Controllers;

use App\Models\Master\Kategori_buku;
use Illuminate\Http\Request;

class Kategori_BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategori_bukus = Kategori_buku::all();
        return view('frontend.form.form_kategori.index',compact('kategori_bukus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.form.form_kategori.kategori_buku');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori_buku = new Kategori_buku;
        $kategori_buku->name = $request->get('name');
        $kategori_buku->description = $request->get('description');
        $kategori_buku->save();

        return redirect('kategori_bukus')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori_buku = Kategori_buku::find($id);
        return view('frontend.form.form_kategori.edit',compact('kategori_buku','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kategori_buku = Kategori_buku::find($id);
        $kategori_buku->name=$request->get('name');
        $kategori_buku->description=$request->get('description');
        $kategori_buku->save();

        return redirect('kategori_bukus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori_buku = Kategori_buku::find($id);
        $kategori_buku->delete();
        return redirect('kategori_bukus')->with('success');
    }
}
