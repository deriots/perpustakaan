<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Response;
use App\Models\Transaksi\Pengembalian;
use App\Models\Transaksi\Peminjaman;
use App\Models\Transaksi\Detail_Pinjam;
use Illuminate\Http\Request;

class PengembalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengembalian = Pengembalian::select('pengembalians.*','users.name as user')
            ->join('users', 'users.id', '=', 'pengembalians.user_id')
            ->get();
        return view('frontend.form.form_pengembalian.index', compact('pengembalian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $peminjaman = Peminjaman::orderBy('code', 'asc')->get();
        $return_code = Pengembalian::Code();
        return view('frontend.form.form_pengembalian.pengembalian', compact('peminjaman', 'return_code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pengembalian = new Pengembalian;
        $pengembalian->loan_code = $request->get('loan_code');
        $pengembalian->return_code = $request->get('return_code');
        $pengembalian->user_id = Auth::user()->id;
        $pengembalian->number_of_books = $request->get('number_of_books');
        $pengembalian->date_back = $request->get('date_back');
        $pengembalian->denda = $request->get('denda');
        $pengembalian->save();

        return redirect('pengembalian')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idheaderpeminjaman)
    {
        $detailpeminjaman = Detail_Pinjam::select('bukus.*', 'kategori_bukus.name as category')
            ->where('loan_id', '=', $idheaderpeminjaman)
            ->join('bukus', 'detail_pinjams.book_id', '=', 'bukus.id')
            ->join('kategori_bukus', 'bukus.category', '=', 'kategori_bukus.id')
            ->get();
        // dd($detailpeminjaman);

        return view('frontend.form.form_pengembalian.detail_peminjaman', compact('detailpeminjaman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengembalian = Pengembalian::find($id);
        $peminjaman = Peminjaman::orderBy('code', 'asc')->get();
        return view('frontend.form.form_pengembalian.edit', compact('pengembalian', 'peminjaman', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pengembalian = Pengembalian::find($id);
        $pengembalian->loan_code = $request->get('loan_code');
        $pengembalian->return_code = $request->get('return_code');
        $pengembalian->number_of_books = $request->get('number_of_books');
        $pengembalian->date_back = $request->get('date_back');
        $pengembalian->denda = $request->get('denda');
        $pengembalian->save();

        return redirect('pengembalian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengembaian = Pengembalian::find($id);
        $pengembaian->delete();
        return redirect('pengembalian')->with('success');
    }

    public function getitem($code)
    {
        $peminjaman = Peminjaman::where('peminjaman.code', $code)
            ->firstOrFail();
        return Response::json($peminjaman);
    }
}
