<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Response;
use App\Models\Master\Anggota_Perpus;
use App\Models\Master\Buku;
use App\Models\Transaksi\Peminjaman;
use App\Models\Transaksi\Detail_Pinjam;
use Illuminate\Http\Request;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjaman = Peminjaman::select('peminjaman.*','anggota__perpuses.name as cuk','users.name as user')
            ->join('anggota__perpuses','anggota__perpuses.id','=','peminjaman.member_id')
            ->join('users','users.id','=','peminjaman.user_id')
            ->get();
        return view('frontend.form.form_peminjaman.index',compact('peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $anggota_perpus = Anggota_Perpus::orderBy('name','asc')->get();
        $code = Peminjaman::Code();
        $bukus = Buku::orderBy('book_title','asc')->get();
        return view('frontend.form.form_peminjaman.peminjaman',compact('anggota_perpus','bukus','code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        DB::beginTransaction();

        try{
            $peminjaman = new Peminjaman;
            $peminjaman->code = $request->get('code');
            $peminjaman->member_id = $request->get('member_id');
            $peminjaman->user_id = Auth::user()->id;
            $peminjaman->loan_date = $request->get('loan_date');
            $peminjaman->estimated_return_date = $request->get('estimated_return_date');
            $peminjaman->number_of_books = $request->get('number_of_books');
            $peminjaman->save();

            $detail = $this->collectDetail($request, $peminjaman->id);

            Detail_Pinjam::insert($detail);

            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }

        return redirect('peminjaman')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idheaderpeminjaman)
    {
        $detailpeminjaman = Detail_Pinjam::select('bukus.*', 'kategori_bukus.name as category')
            ->where('loan_id','=',$idheaderpeminjaman)
            ->join('bukus', 'detail_pinjams.book_id', '=', 'bukus.id')
            ->join('kategori_bukus', 'bukus.category', '=', 'kategori_bukus.id')
            ->get();
        // dd($detailpeminjaman);

        return view('frontend.form.form_peminjaman.detail_peminjaman', compact('detailpeminjaman'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $peminjaman = Peminjaman::find($id);
        $anggota_perpus = Anggota_Perpus::orderBy('name', 'asc')->get();
        $buku = Buku::orderBy('book_title', 'asc')->get();
        return view('frontend.form.form_peminjaman.edit', compact('peminjaman', 'id', 'anggota_perpus' , 'buku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $peminjaman = Peminjaman::find($id);
        $peminjaman->code = $request->get('code');
        $peminjaman->member_id = $request->get('member_id');
        // $peminjaman->user_id = $request->get('user_id');
        $peminjaman->loan_date = $request->get('loan_date');
        $peminjaman->estimated_return_date = $request->get('estimated_return_date');
        $peminjaman->number_of_books = $request->get('number_of_books');
        $peminjaman->save();

        return redirect('peminjaman');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peminjaman = Peminjaman::find($id);
        $peminjaman->delete();
        return redirect('peminjaman')->with('success');
    }

    public function getitem($id)
    {
        $buku = Buku::select('bukus.*','kategori_bukus.name as category')
                ->join('kategori_bukus', 'bukus.category', '=', 'kategori_bukus.id')
                ->where('bukus.id',$id)
                ->firstOrFail();

        return Response::json($buku);
    }

    protected function collectDetail(Request $request, $loanId)
    {
        $i = 0;
        $array = [];

        foreach ($request->bukuid as $rowBookId) {
            $array[$i]['loan_id'] = $loanId;
            $array[$i]['book_id'] = $rowBookId;
            $i++;
        }

        return $array;
    }
}
