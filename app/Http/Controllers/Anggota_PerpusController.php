<?php

namespace App\Http\Controllers;

use App\Models\Master\Anggota_Perpus;
use Illuminate\Http\Request;

class Anggota_PerpusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggota_perpuses = Anggota_Perpus::all();
        return view('frontend.form.form_anggota.index',compact('anggota_perpuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.form.form_anggota.anggota_perpus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anggota_perpus = new Anggota_Perpus;
        $anggota_perpus->name = $request->get('name');
        $anggota_perpus->date_of_birth = $request->get('date_of_birth');
        $anggota_perpus->address = $request->get('address');
        $anggota_perpus->gender = $request->get('gender');
        $anggota_perpus->no_telphone = $request->get('no_telphone');
        $anggota_perpus->save();

        return redirect('anggota_perpuses')->with('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota_perpus = Anggota_Perpus::find($id);
        return view('frontend.form.form_anggota.edit',compact('anggota_perpus','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $anggota_perpus = Anggota_Perpus::find($id);
        $anggota_perpus->name=$request->get('name');
        $anggota_perpus->date_of_birth=$request->get('date_of_birth');
        $anggota_perpus->address=$request->get('address');
        $anggota_perpus->gender=$request->get('gender');
        $anggota_perpus->no_telphone=$request->get('no_telphone');
        $anggota_perpus->save();

        return redirect('anggota_perpuses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota_perpus = Anggota_Perpus::find($id);
        $anggota_perpus->delete();
        return redirect('anggota_perpuses')->with('success');
    }
}
