<?php

namespace App\Models\Traits;

use DB;
/**
 * Transaction Code
 * @param $string
 * @param $column
 */

trait ReturnCode
{
    protected $date, $string, $column;

    public function getCode(string $string, $column = null)
    {
        return $this->setCode($string);
    }

    public function setCode(string $string = null, $column = 'return_code')
    {
        $getLastCode = DB::raw("coalesce(MAX(CAST(RIGHT(" . $column . ", 5)AS INT )),0) as return_code");

        $stringCode = $string . date('my');

        $query = $this->select($getLastCode)->where($column, 'LIKE', '%' . $stringCode . '%')->first();

        $number = sprintf("%'.05d", $query->return_code + 1); //code + 1 & sprintf string

        return $stringCode . $number;
    }
}