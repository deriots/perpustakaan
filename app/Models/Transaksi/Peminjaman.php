<?php

namespace App\Models\Transaksi;

use App\Models\Traits\TransactionCode;
use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    use TransactionCode;

    protected $table = 'peminjaman';

    protected $dates = ['loan_date', 'estimated_return_date','created_at','updated_at'];

    public function scopeCode($query)
    {
        return $this->getCode('PM');
    }
}
