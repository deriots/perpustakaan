<?php

namespace App\Models\Transaksi;

use App\Models\Traits\ReturnCode;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    use ReturnCode;

    protected $table = 'pengembalians';

    protected $dates = ['date_back', 'created_at', 'updated_at'];

    public function scopeCode($query)
    {
        return $this->getCode('PNG');
    }
}
