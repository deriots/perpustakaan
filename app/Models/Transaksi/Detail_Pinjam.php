<?php

namespace App\Models\Transaksi;

use Illuminate\Database\Eloquent\Model;

class Detail_Pinjam extends Model
{
    protected $table = 'detail_pinjams';

    protected $guarded = ['id'];
}
